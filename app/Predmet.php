<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Predmet extends Model
{
    protected $fillable = ['name', 'profId'];
    protected $table='predmet';
    public $timestamps='false';


    public function profesor()
    {
        return $this->belongsTo(User::class,'profId');
    }

    public function ocena()
    {
        return $this->hasMany(Ocena::class,'predmetId');
    }
}
