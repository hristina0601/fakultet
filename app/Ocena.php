<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ocena extends Model
{
    protected $fillable = ['name', 'predmetId','studentId','ocena'];


    protected $table='ocena';
     public $timestamps='false';

    //user koj slusa predmet
    public function student()
    {
        return $this->belongsTo(User::class,'studentId');
    }

    public function komentar()
    {
        return $this->hasMany(Komentar::class,'ocenaId');
    }
    public function predmet(){
        return $this->belongsTo('App\Predmet','predmetId');
    }
}
