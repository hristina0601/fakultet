<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Komentar extends Model
{
    protected $table='komentar';
    // public $timestamps='false';


    public function user()
    {
        return $this->hasMany(User::class,'userId');
    }

    public function ocena()
    {
        return $this->belongsTo(Ocena::class,'ocenaId');
    }
}
