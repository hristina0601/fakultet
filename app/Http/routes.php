<?php


# Home Page
Route::get('/', 'HomeController@index');

# Login View
Route::get('/login', 'Auth\AuthController@login');
Route::post('/login', 'Auth\AuthController@verifyLogin');

# Register View
Route::get('/register', 'Auth\AuthController@register');
Route::post('/register', 'Auth\AuthController@verifyRegister');

# Logout
Route::get('logout', 'Auth\AuthController@logout');


Route::group(['middleware' => ''],function(){
    Route::get('/',array('as'=>'/','uses'=>'HomeController@index'));
});

Route::resource('/admin/predmet','PredmetController');
Route::get('/admin/profesor','AdministratorController@listProfesors');
Route::get('/admin/student','AdministratorController@listStudents');



Route::get('admin/profesor/create','AdministratorController@createProfesor');
Route::get('admin/student/create','AdministratorController@createStudent');


Route::get('/student/predmeti','StudentController@listStudenti');

Route::resource('/admin','AdministratorController');

Route::resource('/student','StudentController');
Route::get('/student/predmeti/','PredmetiController@index');
Route::get('/student/predmeti/{id}','PredmetiController@listStudenti');
Route::resource('/student/predmeti','PredmetiController');

Route::resource('/profesor','ProfesorController');

Route::group(array('before' => 'login'), function(){

    Route::get('/', array('as' => '/', 'uses' => 'HomeController@index'));

});
Route::group(array('before' => 'admin'), function(){

    Route::get('/', array('as' => '/', 'uses' => 'HomeController@index'));

});

/*Route::group(array('before' => 'login'), function(){

    Route::get('/', array('as' => '/', 'uses' => 'HomeController@index'));

    Route::resource('/student', 'AccountController');
    Route::resource('/profesor', 'AccountController');

    Route::get('/home', array('as' => '/home', 'uses' => 'HomeController@index'));


});*/



