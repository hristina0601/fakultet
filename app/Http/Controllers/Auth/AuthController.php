<?php

namespace App\Http\Controllers\Auth;
use DB;
use App\User;
use Illuminate\Support\Facades\Session;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\MessageBag;
use Illuminate\Database\Eloquent\Model;




class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'logout']);
    }


    public function logout() {
        Auth::logout();

        return redirect('/');
    }
    public function login()
    {
        return view('pages.login');
    }

    public function verifyLogin(){


        if (Auth::attempt(['email' => Input::get('email'), 'password' => Input::get('password')], (Input::get('rememberme') == 'on') ? true : false)) {
            $email=Input::get('email');
            $userType = DB::table('users')->where('email', $email)->pluck('type');
            $activ = DB::table('users')->where('email', $email)->pluck('isactiv');

            if($userType=='0')
            {
                return redirect('/admin')->with('alert-success', 'You are now logged in.');

            }elseif($userType=='1' ){
                return redirect('/profesor')->with('alert-success', 'You are now logged in.');
            }
            elseif($userType=='2'){
                return redirect('/student')->with('alert-success', 'You are now logged in.');
            }

        } else {
            // echo "fail";
            $errors = new MessageBag(['password' => ['Email and/or password invalid.']]);
            return redirect()->back()->withErrors($errors)->withInput(Input::except('password'));
        }
    }


    public function register()
    {
        return view('pages.register');
    }


    public function verifyRegister()
    {
        $data = Input::all();


        $validator = $this->validator($data);

        if ($validator->fails()) {

            return redirect('/register')
                ->withErrors($validator)
                ->withInput();
        } else {
            if($data['type']=='0')
            {
                $this->create($data);
                return redirect('/admin');
            }
            elseif($data['type']=="1") {
                $this->create($data);
                return redirect('/profesor');
            }
            elseif($data['type']=="2"){
                $this->create($data);
                return redirect('/student');
            }
        }
    }


    protected function validator(array $data)
    {
        if($data['type']=='1')
        {
            return Validator::make($data, [
                'name' => 'required|max:255',
                'email' => 'required|email|max:255|unique:users',
                'password' => 'required|confirmed|min:6',
                'salary' => 'required|numeric'
            ]);
        }
        elseif($data['type']=='2')
        {
            return Validator::make($data, [

            ]);
        }
        else{return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|confirmed|min:6',

        ]);
        }

    }

    protected function create(array $data)
    {

        if($data['type']=='1')
        {
            return User::create([
                'name' => $data['name'],
                'email' => $data['email'],
                'password' => bcrypt($data['password']),
                'type' => $data['type'],
                'salary'=>$data['salary'],
            ]);
        }
        elseif($data['type']=='2')
        {
            return User::create([
                'name' => $data['name'],
                'email' => $data['email'],
                'password' => bcrypt($data['password']),
                'type' =>$data['type'],
                'year'=>$data['year'],
            ]);
        }
        else{

                return User::create([
                    'name' => $data['name'],
                    'email' => $data['email'],
                    'password' => bcrypt($data['password']),
                    'type' => $data['type'],

                ]);


        }

    }


}
