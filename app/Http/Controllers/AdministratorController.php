<?php

namespace App\Http\Controllers;

use App\User;
use App\Predmet;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
use View;
class AdministratorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {

        return view('admin.index');
    }

    public function listStudents(){

        $users = DB::table('users')->where('type', '2')->get();

        return view('admin.student')->with('users', $users);

    }

    public function listProfesors(){

        $users = DB::table('users')->where('type', '1')->get();

        return view('admin.profesor')->with('users', $users);

    }

 /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //return View::make('admin.create');
    }

    public function createProfesor()
    {
        return View::make('admin.createProfesor');
    }

    public function createStudent()
    {
        return View::make('admin.createStudent');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $profesor=$request->input('salary');
        $student=$request->input('year');
        $tip=0;
        if(isset($profesor))
        {
            $tip=1;
            $this->validate($request,[
                'name' => 'required|max:255',
                'email' => 'required|email|max:255|unique:users',
                'salary' => 'required|numeric',


        ]);

        }
        elseif(isset($student))
        {
            $tip=2;
            $this->validate($request,[
                'name' => 'required|max:255',
                'email' => 'required|email|max:255|unique:users',
                'salary' => 'required|numeric',
                ]);

        }


            User::create([
                'name'      => $request->input('name'),
                'email'     => $request->input('email'),
                'type'      => $tip ,
                'salary' =>$request->input('salary'),
                'year'  =>$request->input('year'),

            ]);
        if($tip=='1'){
            return redirect('admin/profesor');
        }elseif($tip=='2'){
            return redirect('admin/student');
        }


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user=User::find($id);

            return view('admin.edit')->with('user', $user);


    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user=User::find($id);
        $name=$request->input('name');
        $email=$request->input('email');
        $salary=$request->input('salary');
        $year=$request->input('year');
        $activ=$request->input('activ');



        DB::table('users')
            ->where('id', $id)
            ->update(['name' => $name,
                        'email'=>$email,
                        'salary'=>$salary,
                        'year'=>$year,
                        'isactiv'=>$activ
            ]);
        if(isset($salary)){
            return redirect('admin/profesor');
        }
        elseif(isset($year)){
            return redirect('admin/student');
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user=User::find($id);
        $type=$user->type;
        $user->delete();

        if($user->type=="1"){
            return redirect('admin/profesor');
        }
        else{
            return redirect('admin/student');
        }


    }
}
