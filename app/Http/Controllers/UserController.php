<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;
use DB;
use Session;
use App\User;
use Illuminate\Support\Facades\Redirect;

class AccountController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');

    }

    public function index()
    {
        $currentUser = Auth::user();
        $type=$currentUser->type;
        if($type==0){
            return View::make('profesor.profil')->with('currentUser', $currentUser);
        }
        elseif($type==1)
        {
            return View::make('profesor.profil')->with('currentUser', $currentUser);
        }
        elseif($type==2){
            return View::make('student.profil')->with('currentUser', $currentUser);
        }



    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->user = Auth::user();
        $currentUser = User::findOrFail($id);

        return view('student.edit')->with('currentUser',$currentUser);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $currentUser = Auth::user($id);
        $name=$request->input('name');
        $email=$request->input('email');

        $this->validate($request, [
            'name' => 'required|max:255',
            'email' => 'required|max:255|unique:users,email,'.$currentUser->id.'id',
            'password' => 'required|confirmed|min:6',

        ]);
         DB::table('users')
            ->where('id', $id)
            ->update(['name' => $name,
                'email'=>$email,
                'password'  => bcrypt($request->input('password')),
            ]);


        Session::flash('message', 'Профилот на студентот е успешно креиран');
        return Redirect::to('student/');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
