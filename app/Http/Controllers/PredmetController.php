<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
use App\User;
use App\Predmet;

class PredmetController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        $predmeti = DB::table('predmet')->get();

        $profesori=DB::table('users')->select('id','name')->where('type', '1')->get();

        return view('predmet.predmet')->with('profesori', $profesori)->with('predmeti',$predmeti);
    }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
          $profesori = DB::table('users')->where('type','1')->lists('name', 'id');

        return view('predmet.create')->with('profesori',$profesori);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate($request,[
                'name' => 'required|max:255',
        ]);
        Predmet::create([
            'name'      => $request->input('name'),
            'profId'     => $request->input('profId'),

        ]);
        return redirect('admin/predmet');


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $predmet=Predmet::find($id);
        $profesori=DB::table('users')->where('type', '1')->lists('name', 'id');

        return view('predmet.edit')->with('predmet', $predmet)->with('profesori',$profesori);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $predmet=Predmet::find($id);
        $name=$request->input('name');
        $profId=$request->input('profId');


        DB::table('predmet')
            ->where('id', $id)
            ->update(['name' => $name,
                'profId'=>$profId

            ]);

            return redirect('admin/predmet');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $predmet=Predmet::find($id);
         $predmet->delete();
        return redirect('admin/predmet');

    }
}
