--
-- Database: `fakultet`
--

-- --------------------------------------------------------

--
-- Table structure for table `komentar`
--

CREATE TABLE `komentar` (
  `id` int(10) UNSIGNED NOT NULL,
  `komentar` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `userId` int(10) UNSIGNED NOT NULL,
  `ocenaId` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2014_10_12_000000_create_users_table', 1),
('2014_10_12_100000_create_password_resets_table', 1),
('2015_12_08_212126_create_predmet_table', 2),
('2015_12_08_212245_create_oceni_table', 3),
('2015_12_08_212326_create_komentari_table', 4);

-- --------------------------------------------------------

--
-- Table structure for table `ocena`
--

CREATE TABLE `ocena` (
  `id` int(10) UNSIGNED NOT NULL,
  `ocena` int(11) NOT NULL,
  `studentId` int(10) UNSIGNED NOT NULL,
  `predmetId` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `ocena`
--

INSERT INTO `ocena` (`id`, `ocena`, `studentId`, `predmetId`, `created_at`, `updated_at`) VALUES
(1, 6, 22, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 9, 24, 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 6, 24, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `predmet`
--

CREATE TABLE `predmet` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `profId` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `predmet`
--

INSERT INTO `predmet` (`id`, `name`, `profId`, `created_at`, `updated_at`) VALUES
(1, 'Informatika', 21, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 'Matematika', 21, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(5, 'Fizika', 31, '2015-12-13 12:35:36', '2015-12-13 12:35:36');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `type` int(11) DEFAULT NULL,
  `isactiv` tinyint(4) NOT NULL,
  `salary` int(11) DEFAULT NULL,
  `year` int(11) DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `type`, `isactiv`, `salary`, `year`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Informatika', '', '$2y$10$WEysj.88P5jvB8hYbZ9.TO2oEsIlCgWkHBFNi5Na7TrPZlCMpTot.', 0, 0, NULL, NULL, '3z2IcKgRGMmnX8DBsGRmlF5p25Y1YIA2WgQV3omcSbdzyxxVNbQF8JQ8Z3Xt', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(21, 'Jovica', 'jovo@mail.com', '$2y$10$Zo/oOHAYeYYFrm.MnxlU.e6.M0CKY4JJaIHGRhXQjYmqDf.cek.NG', 1, 0, 12587, NULL, 'Mi617IPkQLIYd6GsmWEVIJoryzSDTtqDvbvvSQLJJMBfQNLwMDZi75VRBti5', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(22, 'Toso Hristo', 'toso@mail.com', '$2y$10$OsIbqVYtB36/beLMCeFAFurPcfBZHlK1Rrvck773Enr87bXgQEgca', 2, 1, NULL, 4, 'gDGOWLqhEcm6i2eDcJgEP3apiOkjQsc4d9SzCcsXcUQXs3HqOKL31cP6IpxB', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(23, 'Hristina Todorovska', 'admin@mail.com', '$2y$10$3Xnm13wsh6ywy93X6U/SU.zYB74ueX64L6dTYQeR7pNlMbiCsPsXK', 0, 0, NULL, NULL, 'ls3vLm2ZUWtaAjgYsUUNjWhpE1cF6lGpUyQKdLXKhtoX5uIIwrEnFqRgyPdR', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(24, 'Brena', 'brena@mail.com', '$2y$10$5JGZhzde8oMo4PSwGCLWaOimRfCG3fHB8ypCDnBAZMOOWzFaTL2ky', 2, 0, NULL, 4, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(27, 'Jovan', 'jovan@mail.com', '$2y$10$mVwRnt0cTLOs7vOGmZH0suIrKrtBAW/HD8VgCa6KODnTi3kEi97ge', 1, 0, 25487, NULL, 'Tay0dWCHlgNorQFlSyfw1aPoWMv7c1S6UKA01ePj6dqPc3vQNDapmY5o5A5q', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(28, 'Petra', 'petra@mail.com', '', 1, 0, 1234, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(29, 'Iva', 'ina@mail.com', '', 1, 0, 1568, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(30, 'Jovica', 'jovica@mail.com', '', 1, 0, 1468, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(31, 'Milan', 'milan@mail.com', '', 1, 0, 123456, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(32, 'Ana', 'ana@mail.com', '', 1, 0, 1568, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(34, 'Ivica', 'ivica@mail.com', '', 1, 1, 15000, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `komentar`
--
ALTER TABLE `komentar`
  ADD PRIMARY KEY (`id`),
  ADD KEY `komentar_userid_foreign` (`userId`),
  ADD KEY `komentar_ocenaid_foreign` (`ocenaId`);

--
-- Indexes for table `ocena`
--
ALTER TABLE `ocena`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ocena_profid_foreign` (`studentId`),
  ADD KEY `ocena_predmetid_foreign` (`predmetId`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`),
  ADD KEY `password_resets_token_index` (`token`);

--
-- Indexes for table `predmet`
--
ALTER TABLE `predmet`
  ADD PRIMARY KEY (`id`),
  ADD KEY `predmet_profid_foreign` (`profId`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `komentar`
--
ALTER TABLE `komentar`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ocena`
--
ALTER TABLE `ocena`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `predmet`
--
ALTER TABLE `predmet`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `komentar`
--
ALTER TABLE `komentar`
  ADD CONSTRAINT `komentar_ocenaid_foreign` FOREIGN KEY (`ocenaId`) REFERENCES `ocena` (`id`),
  ADD CONSTRAINT `komentar_userid_foreign` FOREIGN KEY (`userId`) REFERENCES `users` (`id`);

--
-- Constraints for table `ocena`
--
ALTER TABLE `ocena`
  ADD CONSTRAINT `ocena_predmetid_foreign` FOREIGN KEY (`predmetId`) REFERENCES `predmet` (`id`),
  ADD CONSTRAINT `ocena_profid_foreign` FOREIGN KEY (`studentId`) REFERENCES `users` (`id`);

--
-- Constraints for table `predmet`
--
ALTER TABLE `predmet`
  ADD CONSTRAINT `predmet_profid_foreign` FOREIGN KEY (`profId`) REFERENCES `users` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
