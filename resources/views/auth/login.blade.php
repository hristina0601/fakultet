@extends('layouts.public')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-4 col-sm-offset-4">
                <h2>Please sign in</h2>

                <form method="post" action="{{ url('/login') }}">
                    {!! csrf_field() !!}
                    @if(Session::has('error'))
                        <div class="alert-box success">
                            <h2>{{ Session::get('error') }}</h2>
                        </div>
                    @endif

                    <div class="form-group">
                        <label for="exampleInputEmail1">E-mail address</label>
                        <input type="email" class="form-control" id="inputEmail" placeholder="E-mail" name="email" value="{{ old('email') }}" required>
                        <p class="errors">{{$errors->first('email')}}</p>
                    </div>

                    <div class="form-group">
                        <label for="exampleInputPassword1">Password</label>
                        <input type="password" class="form-control" id="inputPassword" placeholder="Password" name="password" required>
                        <p class="errors">{{$errors->first('password')}}</p>
                    </div>

                    <div class="checkbox">
                        <label>
                            <input type="checkbox" value="remember" name="rememberme"> Remember me
                        </label>
                    </div>

                    <div class="form-group">
                        <button class="btn btn-lg btn-primary btn-block" type="submit" name="submitlogin">Sign in</button>
                    </div>

                    <p class="text-center">
                        <a class="btn btn-link" href="{{ url('/auth/password/email') }}">Forgot Your Password?</a>
                        <a class="btn btn-link" href="{{ url('/auth/register') }}">Register</a>
                    </p>

                    @if(Session::has('error'))
                        <div class="alert-box success">
                            <h2>{{Session::get('error')}}</h2>
                        </div>
                    @endif

                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <strong>Whoops!</strong> There were some problems with your input.<br><br>
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                </form>
            </div>
        </div>
    </div>
@endsection