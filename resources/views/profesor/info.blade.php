<!DOCTYPE html>
<html>
<head>
    <style>
        table {
            border-collapse: collapse;
            width: 100%;
        }

        th, td {
            text-align: left;
            padding: 8px;
        }

        tr:nth-child(even){background-color: #f2f2f2}

        th {
            background-color: #4CAF50;
            color: white;
        }
    </style>
</head>
<body>

<h2>Predmeti</h2>

<table>
    <tr>
        <th>Predmeti</th>
        <th></th>

    </tr>
    @foreach($predmeti as $predmet)
        <tr>
            <td>{{$predmet->name}}</td>
            <td>Vidi studenti</td>
        </tr>

    @endforeach

</table>


</body>
</html>