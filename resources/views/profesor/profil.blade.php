<!DOCTYPE html>
<html>
<head>
    <style>
        table {
            border-collapse: collapse;
            width: 100%;
        }

        th, td {
            text-align: left;
            padding: 8px;
        }

        tr:nth-child(even){background-color: #f2f2f2}

        th {
            background-color: #4CAF50;
            color: white;
        }
    </style>
</head>
<body>

<h2>Profil na {{$currentUser->name}}</h2>

<a  href="{{ url('logout') }}">Logout</a>

<table>
    <tr>
        <th><a href="{{'profesor/'.$currentUser->id.'/'}}">Moj Profil</a></th>
        <th><a href="{{'profesor/predmeti'}}">Predmeti</a></th>
        <th></th>
        <th></th>
    </tr>


</table>


</body>
</html>