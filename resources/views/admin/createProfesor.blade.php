
<head>
    <style>
        table {
            border-collapse: collapse;
            width: 100%;
        }

        th, td {
            text-align: left;
            padding: 8px;
        }

        tr:nth-child(even){background-color: #f2f2f2}

        th {
            background-color: #4CAF50;
            color: white;
        }
    </style>
</head>
<body>
<form method="post" action="{{ url('admin') }}">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">

    <h2>Add Profesor</h2>
    <a  href="{{ url('admin/profesor') }}">Back</a>
    <a  href="{{ url('logout') }}">Logout</a>
    </br>

    <table>
        <tr>
            <th>Name</th>
            <th>Email</th>
            <th>Salary</th>


        </tr>


        <tr>
            <td><input type="text" class="form-control"  name="name" value="{{ old('name') }}" required></td>
            <td><input type="email" class="form-control"   name="email" value="{{ old('email') }}" required></td>
            <td><input type="text" name="salary"></td>


        </tr>
        <tr>
            <td colspan="2"><button  type="submit" name="submit">Save</button></td>
            <td colspan="2"> </td>

        </tr>


    </table>
    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
</form>

</body>
