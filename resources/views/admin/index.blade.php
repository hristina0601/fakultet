<!DOCTYPE html>
<html>
<head>
    <style>
        table {
            border-collapse: collapse;
            width: 100%;
        }

        th, td {
            text-align: left;
            padding: 8px;
        }

        tr:nth-child(even){background-color: #f2f2f2}

        th {
            background-color: #4CAF50;
            color: white;
        }
    </style>
</head>
<body>

<h2>Admin Panel</h2></br>
<a  href="{{ url('logout') }}">Logout</a>

<table>
    <tr>
        <th><a  href="{{ url('/admin/profesor') }}">Profesori</a></th>
        <th><a  href="{{ url('admin/student') }}">Studenti</a></th>
        <th><a  href="{{ url('admin/predmet') }}">Predmeti</a></th>
    </tr>

</table>

</body>
</html>