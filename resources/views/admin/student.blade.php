
<head>
    <style>
        table {
            border-collapse: collapse;
            width: 100%;
        }

        th, td {
            text-align: left;
            padding: 8px;
        }

        tr:nth-child(even){background-color: #f2f2f2}

        th {
            background-color: #4CAF50;
            color: white;
        }
    </style>
</head>
<body>

<h2>Admin Panel</h2>
<a  href="{{ url('/admin') }}">Back</a>

<table>
    <tr>
        <th>Name</th>
        <th>Email</th>
        <th>Year</th>
        <th>Activ</th>
        <th></th>
        <th></th>
    </tr>


    @foreach($users as $user)
        <tr>
            <td>{{$user->name}}</td>
            <td>{{$user->email}} </td>
            <td>{{$user->year}}  </td>
            @if($user->isactiv==1)
                <td>Yes</td>
            @else
                <td>No</td>
            @endif
            <td><a href={{url('/admin/'.$user->id.'/edit/')}}>Edit</a> </td>
            <td>

                {!! Form::model($user,['method' => 'DELETE','route'=>['admin.destroy',$user->id]]) !!}
                {!!  Form::hidden('id', $user->id) !!}
                {!!  Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
                {!!  Form::close() !!}
            </td>

        </tr>

    @endforeach




</table>
<a  href="{{ url('admin/profesor/create/') }}">Create</a>

</body>
