
<head>
    <style>
        table {
            border-collapse: collapse;
            width: 100%;
        }

        th, td {
            text-align: left;
            padding: 8px;
        }

        tr:nth-child(even){background-color: #f2f2f2}

        th {
            background-color: #4CAF50;
            color: white;
        }
    </style>
</head>
<body>
{!! Form::model($user,['method' => 'PATCH','route'=>['admin.update',$user->id]]) !!}
    <input type="hidden" name="_token" value="{{ csrf_token() }}">

<h2>Edit</h2>
@if($user->type=='1')
    <a  href="{{ url('admin/profesor') }}">Back</a>
@elseif($user->type=='2')
    <a  href="{{ url('admin/student') }}">Back</a>
@endif

</br>
<a  href="{{ url('logout') }}">Logout</a>
<table>
    <tr>
        <th>Name</th>
        <th>Email</th>
        @if($user->type=='1')
            <th>Salary</th>
        @elseif($user->type=='2')
            <th>Year</th>
        @endif
        <th>Activ (0 or 1)</th>


    </tr>


    <tr>
        <td><input type="text" name="name" value="{{$user->name}}"></td>
        <td><input type="email" name="email"  value="{{$user->email}}" ></td>
        @if($user->type=='1')
        <td><input type="text" name="salary"  value="{{$user->salary}}"></td>
        @elseif($user->type=='2')
             <td><input type="text" name="year"  value="{{$user->year}}"></td>
            @endif
        <td><input type="text" name="activ"  value="{{$user->isactiv}}"></td>
    </tr>
    <tr>
        <td colspan="2"><button  type="submit" name="submit">Save</button></td>
        <td colspan="2"> </td>

    </tr>






</table>
{!! Form::close() !!}

</body>
