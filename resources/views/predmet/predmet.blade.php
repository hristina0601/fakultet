<!DOCTYPE html>
<html>
<head>
    <style>
        table {
            border-collapse: collapse;
            width: 100%;
        }

        th, td {
            text-align: left;
            padding: 8px;
        }

        tr:nth-child(even){background-color: #f2f2f2}

        th {
            background-color: #4CAF50;
            color: white;
        }
    </style>
</head>
<body>

<h2>Predmeti</h2>
<a  href="{{ url('/admin') }}">Back</a>

<table>
    <tr>
        <th>Predmet</th>
        <th>Profesor</th>
        <th></th>
        <th></th>
    </tr>
    @foreach($predmeti as $predmet)
        <tr>
            <td>{{$predmet->name}}</td>
            <td>
                @foreach ($profesori as $profesor)
                @if($predmet->profId == $profesor->id)
                    {{$profesor->name}}

                @endif
                    @endforeach
            </td>
            <td><a href={{url('/admin/predmet/'.$predmet->id.'/edit/')}}>Edit</a> </td>
            <td>

                {!! Form::model($predmet,['method' => 'DELETE','route'=>['admin.predmet.destroy',$predmet->id]]) !!}
                {!!  Form::hidden('id', $predmet->id) !!}
                {!!  Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
                {!!  Form::close() !!}
            </td>

        </tr>

    @endforeach

</table>
<a  href="{{ url('admin/predmet/create/') }}">Create</a>

</body>
</html>