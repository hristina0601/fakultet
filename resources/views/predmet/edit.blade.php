
<head>
    <style>
        table {
            border-collapse: collapse;
            width: 100%;
        }

        th, td {
            text-align: left;
            padding: 8px;
        }

        tr:nth-child(even){background-color: #f2f2f2}

        th {
            background-color: #4CAF50;
            color: white;
        }
    </style>
</head>
<body>
{!! Form::model($predmet,['method' => 'PATCH','route'=>['admin.predmet.update',$predmet->id]]) !!}
<input type="hidden" name="_token" value="{{ csrf_token() }}">

<h2>Edit</h2>

    <a  href="{{ url('admin/predmet') }}">Back</a>


    </br>
    <a  href="{{ url('logout') }}">Logout</a>
    <table>
        <tr>
            <th>Name</th>
            <th>Profesor</th>

        </tr>


        <tr>
            <td><input type="text" name="name" value="{{$predmet->name}}"></td>

            <td>{!! Form::select('profId', $profesori, null, ['class' => 'form-control']) !!}</td>
        </tr>
        <tr>
            <td colspan="2"><button  type="submit" name="submit">Save</button></td>
            <td colspan="2"> </td>

        </tr>






    </table>
    {!! Form::close() !!}

</body>
