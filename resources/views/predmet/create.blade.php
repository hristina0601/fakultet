
<head>
    <style>
        table {
            border-collapse: collapse;
            width: 100%;
        }

        th, td {
            text-align: left;
            padding: 8px;
        }

        tr:nth-child(even){background-color: #f2f2f2}

        th {
            background-color: #4CAF50;
            color: white;
        }
    </style>
</head>
<body>
<form method="post" action="{{ url('admin/predmet') }}">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">


    <a  href="{{ url('/admin/predmet') }}">Back</a>
    <a  href="{{ url('logout') }}">Logout</a>
    <table>
        <tr>
            <th>Name</th>
            <th>Profesor</th>


        </tr>


        <tr>
            <td><input type="text" name="name"></td>
             <td>{!! Form::select('profId', $profesori, null, ['class' => 'form-control']) !!}</td>
        </tr>
        <tr>
            <td colspan="2"><button  type="submit" name="save">Save</button></td>
            <td colspan="2"> </td>

        </tr>






    </table>
</form>

</body>
