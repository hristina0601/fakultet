<!DOCTYPE html>
<html>
<head>
    <style>
        table {
            border-collapse: collapse;
            width: 100%;
        }

        th, td {
            text-align: left;
            padding: 8px;
        }

        tr:nth-child(even){background-color: #f2f2f2}

        th {
            background-color: #4CAF50;
            color: white;
        }
    </style>
</head>
<body>

<h2>Predmeti</h2>
<a  href="{{ url('/student') }}">Back</a>

<table>
    <tr>
        <th>Predmet</th>
        <th>Profesor</th>
        <th></th>
        <th></th>
    </tr>
    @foreach($predmeti as $predmet)
        <tr>
            <td>{{$predmet->name}}</td>
            <td>
                @foreach ($profesori as $profesor)
                    @if($predmet->profId == $profesor->id)
                        {{$profesor->name}}

                    @endif
                @endforeach
            </td>

            <td><button  type="submit" name="submit">Zapisi predmet</button></td>

            <td><a href="{{ url('/student/predmeti/'.$predmet->id) }}">Studenti</a> </td>




        </tr>

    @endforeach

</table>


</body>
</html>