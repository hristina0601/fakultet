@extends('layouts.public')


@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-4 col-sm-offset-4">
                <h2>Register</h2>

                <form method="post" action="{{ url('/register') }}">

                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif


                    <div class="form-group">
                        <label for="name">Name</label>
                        <input type="text" class="form-control" placeholder="Name" name="name" value="{{ old('name') }}" required>
                    </div>

                    <div class="form-group">
                        <label for="exampleInputEmail1">E-mail address</label>
                        <input type="email" class="form-control"  placeholder="E-mail" name="email" value="{{ old('email') }}" required>
                    </div>



                    <div class="form-group">
                        <label for="InputPassword">Password</label>
                        <input type="password" class="form-control"  placeholder="Password" name="password">
                    </div>

                    <div class="form-group">
                        <label for="InputConfirmPassword">Confirm Password</label>
                        <input type="password" class="form-control" placeholder="Confirm Password" name="password_confirmation">
                    </div>



                    <div class="radio">
                        <label for="useType"></label>
                        <input type="radio" name="usertype" onclick="check(this.value)" value="profesor">Profesor
                    </div>

                    <div class="radio">
                        <label for="useType"></label>
                        <input type="radio" name="usertype" onclick="check(this.value)" value="student">Student
                    </div>
                    <div class="form-group" id="student" style="display:none">
                        <label for="InputYear">Year</label>
                        <input type="text" class="form-control"  placeholder="Year" name="year">
                    </div>
                    <div class="form-group" id="profesor" style="display:none">
                        <label for="InputSalary">Salary</label>
                        <input type="text" class="form-control"  placeholder="Salary" name="salary">
                    </div>

                    <input type="hidden" name="type" id="type" value="0">

                    <div class="form-group">
                        <button class="btn btn-lg btn-primary btn-block" type="submit" name="submit">Register</button>

                    </div>

                    <p class="text-center">
                        <a class="btn btn-link" href="{{ url('/login') }}">Back to login page</a>

                    </p>


                </form>
            </div>
        </div>
    </div>

    <script>
        function check(usertype){
            var usertype=document.getElementsByName("usertype");
            for (var i=0; i< usertype.length; i++)
            {
                if(usertype[i].checked==true)
                {
                    selectedUser=usertype[i].value;
                }
            }
            if(selectedUser=="profesor"){
                document.getElementById("profesor").style.display = '';
                document.getElementById("student").style.display = 'none';
                document.getElementById("type").value = '1';
            }
            if(selectedUser=="student"){
                document.getElementById("student").style.display = '';
                document.getElementById("profesor").style.display = 'none';
                document.getElementById("type").value = '2';
            }

        }

    </script>
@endsection