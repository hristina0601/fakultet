
<head>
    <style>
        table {
            border-collapse: collapse;
            width: 100%;
        }

        th, td {
            text-align: left;
            padding: 8px;
        }

        tr:nth-child(even){background-color: #f2f2f2}

        th {
            background-color: #4CAF50;
            color: white;
        }
    </style>
</head>
<body>
{!! Form::model($currentUser,['method' => 'PATCH','route'=>['student.update',$currentUser->id]]) !!}
<input type="hidden" name="_token" value="{{ csrf_token() }}">

<h2>Edit Profil</h2>

    </br>
    <a  href="{{ url('logout') }}">Logout</a>
@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
    <table>
        <tr>
            <th>Name</th>
            <th>Email</th>
            <th>Password</th>
            <th>Confirm Password</th>
        </tr>
        <tr>
            <td><input type="text" name="name" value="{{$currentUser->name}}"></td>
            <td><input type="email" name="email"  value="{{$currentUser->email}}" ></td>
            <td><input type="password" name="password"></td>
            <td><input type="password" name="password_confirmation"></td>
        </tr>
        <tr>
            <td colspan="2"><button  type="submit" name="submit">Save</button></td>
            <td colspan="2"> </td>

        </tr>






    </table>
    {!! Form::close() !!}

</body>
