<!DOCTYPE html>
<html>
<head>
    <style>
        table {
            border-collapse: collapse;
            width: 100%;
        }

        th, td {
            text-align: left;
            padding: 8px;
        }

        tr:nth-child(even){background-color: #f2f2f2}

        th {
            background-color: #4CAF50;
            color: white;
        }
    </style>
</head>
<body>

<h2>Studenti</h2>
<a  href="{{ url('/student/predmeti/') }}">Back</a>

<table>
    <tr>
        <th>Student</th>

    </tr>

    @foreach($studenti as $student)
        @foreach($oceni as $ocena)
            @if($student->id == $ocena->studentId )
                    <tr><td>{{$student->name}}</td></tr>
             @endif
        @endforeach
    @endforeach

</table>


</body>
</html>