<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOceniTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ocena', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ocena');
            $table->integer('studentId')->unsigned();
            $table->foreign('studentId')->references('id')->on('users');
            $table->integer('predmetId')->unsigned();
            $table->foreign('predmetId')->references('id')->on('predmet');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('oceni');
    }
}
